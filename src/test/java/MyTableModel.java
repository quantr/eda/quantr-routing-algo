/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

import hk.quantr.routingalgo.lee.Path;
import hk.quantr.routingalgo.lee.Point;
import java.util.ArrayList;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author michelle <hawhhc@gmail.com>
 */
public class MyTableModel extends DefaultTableModel {

	public ArrayList<Path> paths = new ArrayList();

	@Override
	public Object getValueAt(int row, int column) {
		return null;
	}

	@Override
	public int getColumnCount() {
		return 2;
	}

	@Override
	public int getRowCount() {
		if (paths == null) {
			return 0;
		}
		return paths.size();
	}

}
