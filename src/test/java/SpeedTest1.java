
import hk.quantr.routingalgo.lee.Path;
import hk.quantr.routingalgo.lee.VerifyGrid;

/**
 *
 * @author Peter (peter@quantr.hk)
 */
public class SpeedTest1 {

	public static void main(String args[]) {
		VerifyGrid grid = new VerifyGrid(100);
		for (int x = 0; x < grid.nodeLoc.length; x++) {
			for (int y = 0; y < grid.nodeLoc[0].length; y++) {
				grid.nodeLoc[x][y].applied = false;
			}
		}
//		System.out.println(grid.findPaths(new hk.quantr.routingalgo.lee.Point(0, 0), new hk.quantr.routingalgo.lee.Point(20, 20)));
		grid.findPaths(new hk.quantr.routingalgo.lee.Point(0, 0), new hk.quantr.routingalgo.lee.Point(10, 10));

		Path p = grid.findPath(new hk.quantr.routingalgo.lee.Point(0, 0), new hk.quantr.routingalgo.lee.Point(99, 99), false);
		System.out.println(p);
	}
}
