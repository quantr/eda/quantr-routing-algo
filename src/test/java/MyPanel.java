
import hk.quantr.routingalgo.lee.Path;
import hk.quantr.routingalgo.lee.Point;
import hk.quantr.routingalgo.lee.VerifyGrid;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.util.ArrayList;
import javax.swing.JPanel;

/**
 *
 * @author Peter (peter@quantr.hk)
 */
public class MyPanel extends JPanel {

	int gridSize = 100;
	int gridWidth = 20;
	public VerifyGrid grid;
	public ArrayList<Path> paths = new ArrayList();

	@Override
	public void paint(Graphics g) {
		if (grid != null) {
			g.setColor(Color.decode("#eeeeee"));
			for (int x = 0; x < grid.size; x++) {
				for (int y = 0; y < grid.size; y++) {
					g.fillRect(x * gridWidth, y * gridWidth, gridWidth, gridWidth);
				}
			}

			// draw
			g.setColor(Color.blue);
			if (!paths.isEmpty()) {
				for (int index = 0; index < paths.size(); index++) {
					Path p = paths.get(index);
					if (!p.corners.isEmpty()) {
						drawLine(g, index, p.start, p.corners.get(0));
						for (int i = 1; i < p.corners.size(); i++) {
							drawLine(g, index, p.corners.get(i), p.corners.get(i - 1));
						}
						Point c = p.corners.get(p.corners.size() - 1);
						drawLine(g, index, c, p.end);

					} else {
						drawLine(g, index, p.end, p.start);
					}
				}
			}

			g.setColor(Color.red);
			for (int x = 0; x < grid.size; x++) {
				for (int y = 0; y < grid.size; y++) {
					if (grid.nodeLoc[x][y].applied) {
						g.fillRect(x * gridWidth, y * gridWidth, gridWidth, gridWidth);
					}
//					if (grid.wireCol[x][y]) {
//						g.drawLine(x * gridWidth, (y + 1) * gridWidth, x * gridWidth, y * gridWidth);
//					}
//					if (grid.wireRow[x][y]) {
//						g.drawLine(x * gridWidth, y * gridWidth, x * gridWidth + gridWidth, y * gridWidth);
//					}
				}
			}
//		for (int x = 0; x < size; x++) {
//			for (int y = 0; y < size; y++) {
//				if (wireCol[x][y]) {
//					g.setColor(Color.black);
////					if (findEdge(x, y, false) != null) {
////						findEdge(x, y, false).paint(g);
////					}
//					g.drawLine(x * canvas.gridSize, (y + 1) * canvas.gridSize, x * canvas.gridSize, y * canvas.gridSize);
//				}
//				if (wireRow[x][y]) {
//					g.setColor(Color.black);
////					if (findEdge(x, y, true) != null) {
////						findEdge(x, y, true).paint(g);
////					}
//					g.drawLine(x * canvas.gridSize, y * canvas.gridSize, x * canvas.gridSize + canvas.gridSize, y * canvas.gridSize);
//				}
//				g.setColor(Color.blue);
//				if (nodeLoc[x][y].applied) {
//					g.fillOval(x * canvas.gridSize, y * canvas.gridSize, 5, 5);
//				}
//				if (ports[x][y].getConnectionState()) {
//					ports[x][y].paint(g, canvas.gridSize);
//				}
//			}
//		}
			g.setColor(Color.black);
			for (int x = 0; x < gridSize; x++) {
				g.drawLine(x * gridWidth, 0, x * gridWidth, getHeight());
			}
			for (int y = 0; y < gridSize; y++) {
				g.drawLine(0, y * gridWidth, getWidth(), y * gridWidth);
			}
			g.setColor(Color.red);
			for (int x = 0; x < grid.size; x++) {
				for (int y = 0; y < grid.size; y++) {
//					if (grid.nodeLoc[x][y].applied) {
//						g.fillRect(x * gridWidth, y * gridWidth, gridWidth, gridWidth);
//					}
					if (grid.wireCol[x][y]) {
						g.drawLine(x * gridWidth + gridWidth / 2, (y + 1) * gridWidth + gridWidth / 2, x * gridWidth + gridWidth / 2, y * gridWidth + gridWidth / 2);
					}
					if (grid.wireRow[x][y]) {
						g.drawLine(x * gridWidth + gridWidth / 2, y * gridWidth + gridWidth / 2, x * gridWidth + gridWidth + gridWidth / 2, y * gridWidth + gridWidth / 2);
					}
				}
			}
		}
	}

	public void drawLine(Graphics g, int index, Point start, Point end) {
		if (start.y == end.y) {
			if (start.x < end.x) {
				for (int x = start.x; x <= end.x; x++) {
					g.setColor(Color.blue);
					g.fillRect(x * gridWidth, start.y * gridWidth, gridWidth, gridWidth);
					g.setColor(Color.white);
					g.drawString(String.valueOf(index), x * gridWidth + (gridWidth / 2), start.y * gridWidth + (gridWidth / 2));
				}
			} else {
				for (int x = end.x; x <= start.x; x++) {
					g.setColor(Color.blue);
					g.fillRect(x * gridWidth, start.y * gridWidth, gridWidth, gridWidth);
					g.setColor(Color.white);
					g.drawString(String.valueOf(index), x * gridWidth + (gridWidth / 2), start.y * gridWidth + (gridWidth / 2));
				}
			}
		} else if (start.y < end.y) {
			for (int y = start.y; y <= end.y; y++) {
				g.setColor(Color.blue);
				g.fillRect(start.x * gridWidth, y * gridWidth, gridWidth, gridWidth);
				g.setColor(Color.white);
				g.drawString(String.valueOf(index), start.x * gridWidth + (gridWidth / 2), y * gridWidth + (gridWidth / 2));
			}
		} else {
			for (int y = end.y; y <= start.y; y++) {
				g.setColor(Color.blue);
				g.fillRect(start.x * gridWidth, y * gridWidth, gridWidth, gridWidth);
				g.setColor(Color.white);
				g.drawString(String.valueOf(index), start.x * gridWidth + (gridWidth / 2), y * gridWidth + (gridWidth / 2));
			}
		}
	}
}
