/*
 * Copyright 2023 ken.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hk.quantr.routingalgo.lee;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;

/**
 *
 * @author ken
 */
public class VerifyGrid {

	int numOfMove = 0;
	int numberOfCorners = 1000;
	Point end;
	Point start;
	public final int[][] direction = {{0, -1}, {0, 1}, {-1, 0}, {1, 0}};
	int[] box = {0, 0, 0, 0};
//    int[][] wireDirection = {{0, -1}, {0, 1}, {-1, 0}, {1, 0}}; //using nextpoint
	public static boolean arrived = false;
	int moveDir;
	public boolean quickmode = false;
	//public Node[][] nodeLoc = new Node[200][200];
	public ArrayList<Path> pathList;
	public Node[][] nodeLoc;
	public boolean[][] wireCol;
	public boolean[][] wireRow;
	public int size;
	SimpleDateFormat sdf = new SimpleDateFormat("mm:ss.SSS");
	boolean shouldRun = false;
	ArrayList<ArrayList<Node>> stepList = new ArrayList<ArrayList<Node>>();
	int threshold = 10;

	public VerifyGrid(int size) {
		this.size = size;
		clear();
	}

	public void initialize() { // for searching new path
		for (int x = 0; x < size; x++) {
			for (int y = 0; y < size; y++) {
				nodeLoc[x][y].steps = 10000;
			}
		}
		pathList.clear();
		this.numberOfCorners = 1000;
		arrived = false;
		numOfMove = 0;

		this.stepList.clear();
	}

	public final void clear() {
		nodeLoc = new Node[size][size];
		wireCol = new boolean[size][size];
		wireRow = new boolean[size][size];
		pathList = new ArrayList();
		this.numberOfCorners = 1000;
		arrived = false;
		for (int x = 0; x < size; x++) {
			for (int y = 0; y < size; y++) {
				nodeLoc[x][y] = new Node(x, y);
				wireCol[x][y] = false;
				wireRow[x][y] = false;
			}
		}
	}

	public void setnodeLoc(int x, int y) {
		nodeLoc[x][y].applied = true;
	}

	public void setWireCol(int x, int y) {

	}

	public boolean nodeIfApplied(int x, int y) {
		return nodeLoc[x][y].applied;
	}

	public boolean wireColIfApplied(int x, int y) {
		return wireCol[x][y];
	}

	public boolean wireRowIfApplied(int x, int y) {
		return wireRow[x][y];
	}

	public void start(boolean inBox) {
		Point point = start;
		nodeLoc[point.x][point.y].steps = 0;
		move(inBox);

	}

	public void move(boolean inBox) {
		Node nodeTemp = new Node(0, 0);
		ArrayList<Node> temp = new ArrayList();
		ArrayList<Node> s = new ArrayList();
		nodeLoc[start.x][start.y].steps = 0;
		temp.add(nodeLoc[start.x][start.y]);
		this.stepList.add(temp);
		s = stepList.get(0);
		int count = 0;
		while (!arrived) {
			if (count >= stepList.size()) {
				break;
			}
			s = stepList.get(count);
			if (!s.isEmpty()) {
				ArrayList<Node> temp1 = new ArrayList();
//				System.out.println("ran");
//				System.out.println(s.size());
				for (int e = 0; e < s.size(); e++) {
//					System.out.println("ran");
					Point point = s.get(e).location;
					for (int i = 0; i < 4; i++) {
						int x = point.x + direction[i][0]; //next point 
						int y = point.y + direction[i][1];
						if (x >= 0 && x < size && y >= 0 && y < size) { // check if it touches boundaries y - 1 >= 0
							if (nodeLoc[x][y].steps > nodeLoc[point.x][point.y].steps + 1) {// check suitable step
								if (!inBox || (x > box[0] && x < box[1] && y > box[2] && y < box[3])) { //move in box
									if (!nodeLoc[x][y].applied || (x == end.x && y == end.y)) {// check if the next point is not used or it is end point
//										 && point.y + direction[i][1] >= 0 && point.y + direction[i][1] < this.size && 
										if ((i == 0 && !wireColIfApplied(point.x, y))
												|| (i == 1 && !wireColIfApplied(point.x, point.y))
												|| (i == 2 && !wireRowIfApplied(x, point.y))
												|| (i == 3 && !wireRowIfApplied(point.x, point.y))) //												|| (direction[i][1] == 0 && point.x + direction[i][0] >= 0 && point.x + direction[i][0] < this.size && !wireRowIfApplied(point.x + direction[i][0], y))) 
										{
											// search suitable path (wire)
											if (nodeLoc[point.x][point.y].steps + 1 <= nodeLoc[end.x][end.y].steps) { // check if next steps alr exceed maximum steps
												nodeLoc[x][y].steps = nodeLoc[point.x][point.y].steps + 1; // mark the steps used 
												if (point.x != end.x || point.y != end.y) {
													temp1.add(nodeLoc[x][y]);
												}
												if (x == end.x && y == end.y) {
													arrived = true;
												}
												printStep();
											}
										}
									}
								}

							}
						}

					}
				}
				stepList.add(temp1);
				count++;
			} else {
				return;
			}

		}

	}

	public void search(Point prevPoint, Point point, IPath path) {
//		System.out.println("search 1 " + sdf.format(new Date()));
		if (arrived && quickmode) {
//			System.out.println("search 4 " + sdf.format(new Date()));
			return;
		}
		for (int i = 0; i < 4; i++) {
			int x = point.x + direction[i][0]; //next point 
			int y = point.y + direction[i][1];
			if (x >= 0 && x < size && y >= 0 && y < size) {
				if (nodeLoc[x][y].steps == 0) { //end case 
//					System.out.println("stepped on endPoint");
					if (!(point.y - prevPoint.y == direction[i][1] && point.x - prevPoint.x == direction[i][0])) {//check if only step 2 steps and need to turn
						Path tempPath = new Path(end, point, path.getCorners());
						tempPath.corners.add(point);
					}
					Path tempPath = new Path(path.getStart(), path.getEnd(), (ArrayList<Point>) path.getCorners().clone());
//					System.out.println(pathList);
					pathList.add(new Path(end, new Point(x, y), tempPath.getCorners()));
					arrived = true;
					if (path.getCorners().size() < numberOfCorners) {
						numberOfCorners = path.getCorners().size();
					}
					return;
				} else if (nodeLoc[x][y].steps == nodeLoc[point.x][point.y].steps - 1) { //search suitable node
					if ((i == 0 && wireColIfApplied(point.x, y))
							|| (i == 1 && wireColIfApplied(point.x, point.y))
							|| (i == 2 && wireRowIfApplied(x, point.y))
							|| (i == 3 && wireRowIfApplied(point.x, point.y))) //												|| (direction[i][1] == 0 && point.x + direction[i][0] >= 0 && point.x + direction[i][0] < this.size && !wireRowIfApplied(point.x + direction[i][0], y))) 
					{
					} else if (point.y - prevPoint.y == direction[i][1] && point.x - prevPoint.x == direction[i][0] || (prevPoint.x == point.x && prevPoint.y == point.y)) { //check if it turned to other direction
						Path tempPath = new Path(path.getStart(), path.getEnd(), (ArrayList<Point>) path.getCorners().clone());
						search(point, new Point(x, y), new Path(end, point, tempPath.getCorners()));
					} else {
						if (path != null && path.getCorners().size() > 3 && !pathList.isEmpty()) {
//								System.out.println("search 3 " + sdf.format(new Date()));
							return;
						}
						Path tempPath = new Path(path.getStart(), path.getEnd(), (ArrayList<Point>) path.getCorners().clone());
						tempPath.getCorners().add(point);
						search(point, new Point(x, y), new Path(end, point, tempPath.getCorners()));
					}
//					}
				}
			}
		}
//		System.out.println("search 2 " + sdf.format(new Date()));
	}

	public ArrayList<ArrayList<Node>> searchPaths(Point end) {
		ArrayList<ArrayList<Node>> paths = new ArrayList();
		HashMap<Node, ArrayList<ArrayList<Node>>> pathMap = new HashMap();
		ArrayList<Node> nodeToBeSearched = new ArrayList();
		ArrayList<Node> searchedNode = new ArrayList();
		nodeToBeSearched.add(nodeLoc[end.x][end.y]);
		searchedNode.add(nodeLoc[end.x][end.y]);
		ArrayList<Node> init = new ArrayList();
		init.add(nodeLoc[end.x][end.y]);
		ArrayList<ArrayList<Node>> temps = new ArrayList();
		temps.add(init);
		pathMap.put(nodeLoc[end.x][end.y], temps);

		while (!nodeToBeSearched.isEmpty()) {
			Node node = nodeToBeSearched.get(0);
			nodeToBeSearched.remove(0);
//			System.out.println("searching" + node);
			for (int i = 0; i < 4; i++) {
				int x = node.location.x + direction[i][0]; //next point 
				int y = node.location.y + direction[i][1];
				if (x != -1 && y != -1) {
//					System.out.println("next point " + nodeLoc[x][y]);
					if (nodeLoc[x][y].steps == node.steps - 1 && nodeLoc[x][y].steps != 0) {
//						System.out.println("searching");
						if (!searchedNode.contains(nodeLoc[x][y])) {
							nodeToBeSearched.add(nodeLoc[x][y]);
							searchedNode.add(nodeLoc[x][y]);
						}
						ArrayList<ArrayList<Node>> pathToNextPoint = new ArrayList();
						if (pathMap.containsKey(nodeLoc[x][y])) {
							pathToNextPoint = pathMap.get(nodeLoc[x][y]);
						}

						for (int k = 0; k < pathMap.get(node).size(); k++) {
							ArrayList<Node> p = (ArrayList<Node>) pathMap.get(node).get(k).clone();
							p.add(nodeLoc[x][y]);
							pathToNextPoint.add(p);
						}
//							path.add(nodeLoc[x][y]);
//						System.out.println("path " + Arrays.deepToString(pathToNextPoint.toArray()));
						pathMap.put(nodeLoc[x][y], pathToNextPoint);
					} else if (nodeLoc[x][y].steps == 0) {
						for (int k = 0; k < pathMap.get(node).size(); k++) {
							ArrayList<Node> p = (ArrayList<Node>) pathMap.get(node).get(k).clone();
							p.add(nodeLoc[x][y]);
							paths.add(p);
							if (paths.size() >= 10) {
								return paths;
							}
						}
					}
//							ArrayList<Node> path = (ArrayList<Node>) pathMap.get(node).clone();
//							path.add(nodeLoc[x][y]);
//							paths.add(path);
				}
			}
			pathMap.remove(node);
		}

//		System.out.println(Arrays.deepToString(pathMap.keySet().toArray()));
		return paths;
	}

	public ArrayList<Path> findPaths(Point start, Point end) {
		if (start.x < 0 || start.y < 0 || end.x < 0 || end.y < 0) {
			return null;
		}
		ArrayList<Path> paths = new ArrayList();
//		System.out.println("findPath 1 " + sdf.format(new Date()));
		shouldRun = true;
		initialize();
		this.start = start;
		this.end = end;
		this.quickmode = false;
		if (start.x > end.x) {
			box[0] = end.x - threshold;
			box[1] = start.x + threshold;
		} else {
			box[0] = start.x - threshold;
			box[1] = end.x + threshold;
		}
		if (start.y > end.y) {
			box[2] = end.y - threshold;
			box[3] = start.y + threshold;
		} else {
			box[2] = start.y - threshold;
			box[3] = end.y + threshold;
		}
		start(true);
		Path path = new Path(end, end, new ArrayList());
		arrived = false;
		search(end, end, path);
//		ArrayList<ArrayList<Node>> tempPaths = searchPaths(end);
//		System.out.println(Arrays.deepToString(tempPaths.toArray()));
//		System.out.println(tempPaths.size());
//		for (int i = 0; i < tempPaths.size(); i++) {
//			for
//		}
		paths = (ArrayList<Path>) pathList.clone();
		paths.sort(Comparator.comparing(Path::getNumOfCorner));
//		System.out.println("???" +paths.size());
		return paths.isEmpty() ? null : paths;
	}

	public Path findPath(Point start, Point end, boolean quick) {
		if (start.x < 0 || start.y < 0 || end.x < 0 || end.y < 0) {
			return null;
		}
		Node testnod = new Node(0, 0);

//		System.out.println("findPath 1 " + sdf.format(new Date()));
		shouldRun = true;
		initialize();
		this.start = start;
		this.end = end;
		this.quickmode = quick;
		if (start.x > end.x) {
			box[0] = end.x - threshold;
			box[1] = start.x + threshold;
		} else {
			box[0] = start.x - threshold;
			box[1] = end.x + threshold;
		}
		if (start.y > end.y) {
			box[2] = end.y - threshold;
			box[3] = start.y + threshold;
		} else {
			box[2] = start.y - threshold;
			box[3] = end.y + threshold;
		}
		start(true);
		Path path = new Path(end, end, new ArrayList());
		arrived = false;
		printStep();
		search(end, end, path);
		pathList.sort(Comparator.comparing(Path::getNumOfCorner));
//		System.out.println(pathList);
		System.out.println(pathList.size());
		if (!pathList.isEmpty()) {
			return pathList.get(0);
		} else {
			initialize();
			start(false);
			arrived = false;
			path = new Path(end, end, new ArrayList());
			search(end, end, path);
			pathList.sort(Comparator.comparing(Path::getNumOfCorner));
			if (!pathList.isEmpty()) {
				return pathList.get(0);
			}
		}
		return null;
	}

	public void printStep() {
//		for (int y = 0; y < 20; y++) {
//			for (int x = 0; x < 25; x++) {
//
//				String txt = Integer.toString(nodeLoc[x][y].steps);
//				System.out.print(txt + " ".repeat(6 - txt.length()));
//			}
//			System.out.print('\n');
//
//		}
//		System.out.print('\n');
	}

	public void applyPath(Path p) { // apply the path found if its called 
		this.nodeLoc[p.start.x][p.start.y].applied = true;
		this.nodeLoc[p.end.x][p.end.y].applied = true;
		if (!p.corners.isEmpty()) {
			applyLine(p.start, p.corners.get(0));
			this.nodeLoc[p.corners.get(0).x][p.corners.get(0).y].applied = true;
			for (int i = 1; i < p.corners.size(); i++) {
				applyLine(p.corners.get(i), p.corners.get(i - 1));
				this.nodeLoc[p.corners.get(i).x][p.corners.get(i).y].applied = true;
			}
			applyLine(p.corners.get(p.corners.size() - 1), p.end);
		} else {
			applyLine(p.start, p.end);
		}
	}

	public void applyLine(Point start, Point end) {
		this.nodeLoc[start.x][start.y].applied = true;
		this.nodeLoc[end.x][end.y].applied = true;
		if (start.y == end.y) {
			if (start.x < end.x) {
				for (int x = start.x; x < end.x; x++) {
					this.wireRow[x][end.y] = true;
				}
			} else {
				for (int x = end.x; x < start.x; x++) {
					this.wireRow[x][end.y] = true;
				}
			}
		} else if (start.y < end.y) {
			for (int y = start.y; y < end.y; y++) {
				this.wireCol[end.x][y] = true;
			}
		} else {
			for (int y = end.y; y < start.y; y++) {
				this.wireCol[end.x][y] = true;
			}
		}
	}

	public boolean isReachable(Point start, Point end) {
		if (start.x < 0 || start.y < 0 || end.x < 0 || end.y < 0) {
			return false;
		}
		ArrayList<Path> paths = new ArrayList();
//		System.out.println("findPath 1 " + sdf.format(new Date()));
		shouldRun = true;
		initialize();
		this.start = start;
		this.end = end;
		this.quickmode = false;
		if (start.x > end.x) {
			box[0] = end.x - threshold;
			box[1] = start.x + threshold;
		} else {
			box[0] = start.x - threshold;
			box[1] = end.x + threshold;
		}
		if (start.y > end.y) {
			box[2] = end.y - threshold;
			box[3] = start.y + threshold;
		} else {
			box[2] = start.y - threshold;
			box[3] = end.y + threshold;
		}
		start(true);
		return arrived;
	}
//	public void unApplyLine(Point start, Point end) {
//		this.nodeLoc[start.x][start.y].applied = false;
//		this.nodeLoc[end.x][end.y].applied = false;
//		if (start.y == end.y) {
//			if (start.x < end.x) {
//				for (int x = start.x; x < end.x; x++) {
//					this.wireRow[x][end.y] = false;
//				}
//			} else {
//				for (int x = end.x; x < start.x; x++) {
//					this.wireRow[x][end.y] = false;
//				}
//			}
//		} else if (start.y < end.y) {
//			for (int y = start.y; y < end.y; y++) {
//				this.wireCol[end.x][y] = false;
//			}
//		} else {
//			for (int y = end.y; y < start.y; y++) {
//				this.wireCol[end.x][y] = false;
//			}
//		}
//	}
}
