package hk.quantr.routingalgo.lee;

import java.util.ArrayList;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public interface IPath {
	
	public String toString();

	public String getName() ;

	public ArrayList<Point> getCorners();

	public Point getStart();

	public Point getEnd() ;
}
