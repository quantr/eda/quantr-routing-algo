/*
 * Copyright 2023 ken.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hk.quantr.routingalgo.lee;

import java.util.ArrayList;

/**
 *
 * @author ken
 */
public class Path implements IPath {

	public String name;
	public ArrayList<Point> corners;
	public Point start, end;

	public Path(Point start, Point end, ArrayList<Point> corners) {
		this.start = start;
		this.end = end;
		this.corners = corners;
	}

	@Override
	public String toString() {
		return "Path{" + "corners=" + corners + ", start=" + start + ", end=" + end + '}';
	}

	public String getName() {
		return name;
	}

	public ArrayList<Point> getCorners() {
		return corners;
	}

	public Point getStart() {
		return start;
	}

	public Point getEnd() {
		return end;
	}

	public int getNumOfCorner() {
		if (this.corners != null) {
			return this.corners.size();
		}
		return 0;
	}

	@Override
	public boolean equals(Object obj) {
		Path p = (Path) obj;
		if (this.start.equals(p.start) && this.end.equals(p.end)) {
			return true;
		} else if (this.end.equals(p.start) && this.start.equals(p.end)) {
			return true;
		}
		return false;
	}
}
