This is the supporting project of [Quantr Logic](https://www.quantr.foundation/project/?project=Quantr%20Logic) , providing the implement of Lee Algorithm

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/YUE0k2uVPFs" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

<figure class="video_container">
  <iframe src="https://www.youtube.com/watch?v=QD4yv-kWT78" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
